SDP Receive integration
=======================

A integration repository for the SDP visibility receive pipeline,
allowing for automated integration level tests of each component,
plus straight-forward performance analysis of the full pipeline
without complicated orchestration getting in the way (e.g. Kubernetes).

Requirements
------------

* Python 3.10 or 3.11
* Poetry 1.3.1 or greater

Get Started
-----------

Clone the repository, then ``poetry install`` to
install all dependencies to a virtual environment.

To run the tests, execute ``make python-test``.