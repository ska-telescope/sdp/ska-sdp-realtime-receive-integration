include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINE_LENGTH = 99
PYTHON_VARS_AFTER_PYTEST = --benchmark-skip
CASACORE_MEASURES_DIR ?= /usr/share/casacore/data

# So all our tests don't need to be in modules
# https://github.com/PyCQA/pylint/pull/5682
PYTHON_SWITCHES_FOR_PYLINT = --recursive=true

python-pre-test:
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"

python-benchmark: PYTHON_VARS_AFTER_PYTEST = --benchmark-only --benchmark-autosave
python-benchmark: python-test

docs-serve:
	sphinx-autobuild docs/src docs/build