#!/usr/bin/env python3

# pylint: disable=invalid-name,redefined-builtin,missing-module-docstring

import os
import sys

from sphinx_pyproject import SphinxConfig

sys.path.insert(0, os.path.abspath("../../src/"))

# -- Project information -----------------------------------------------------

pyproject = globals()
config = SphinxConfig("../../pyproject.toml", globalns=pyproject)

project = pyproject["name"]
copyright = "2023, SKA SDP Developers"
author = pyproject["author"]

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.intersphinx",
]

autodoc_mock_imports = []

html_theme = "sphinx_rtd_theme"
html_context = {
    "favicon": "img/favicon.ico",
    "logo": "img/logo.jpg",
    "theme_logo_only": True,
}
htmlhelp_basename = "developerskatelescopeorgdoc"

intersphinx_mapping = {
    "python": ("https://docs.python.org/", None),
    "ska-sdp-realtime-receive-core": (
        "https://developer.skao.int/projects/ska-sdp-realtime-receive-core/en/latest",
        None,
    ),
    "ska-sdp-cbf-emulator": (
        "https://developer.skao.int/projects/ska-sdp-cbf-emulator/en/latest",
        None,
    ),
    "ska-sdp-realtime-receive-modules": (
        "https://developer.skao.int/projects/ska-sdp-realtime-receive-modules/en/latest",
        None,
    ),
    "ska-sdp-realtime-receive-processors": (
        "https://developer.skao.int/projects/ska-sdp-realtime-receive-processors/en/latest",
        None,
    ),
}

autosectionlabel_prefix_document = True
