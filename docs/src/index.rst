SDP Realtime Receive Integration
================================

This project is an integration point for each part of the
SDP realtime receive pipeline, intended to allow for high
level tests and easy performance analysis of the system
as a whole without the complexity of an orchestration
environment such as Kubernetes.

Context
-------

At a high level the *CBF Emulator*
(:doc:`ska-sdp-cbf-emulator:index`),
pretending to be the Central Signal Processor / Correlator Beam Former,
sends SPEAD2 heaps over the network to
*Receiver* (:doc:`ska-sdp-realtime-receive-modules:index`),
which then unpacks them to the *Plasma Memory Store*,
which is then read by one or more
*Processors*  (:doc:`ska-sdp-realtime-receive-processors:index`)
to do some work (e.g. write visibilities to a measurement set).

There are many potential bottlenecks in this process, and running
the entire pipeline together should make it easier to identify
which are the most important.

.. This diagram can be edited by opening the file directly in https://app.diagrams.net
.. figure:: _static/img/realtime-receive.drawio.svg

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Developer Quickstart <README>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
