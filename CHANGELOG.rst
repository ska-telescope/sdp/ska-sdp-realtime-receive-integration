Changelog
#########

Development
-----------

1.0.0
-----

* Bump dependency on
  ``ska-sdp-cbf-emulator`` to ``^9.0.0``,
  ``ska-sdp-realtime-receive-modules`` to ``^6.0.0``,
  and ``ska-sdp-realtime-receive-processors`` to ``^3.0.0``
  to support SDP v1.

0.3.2
-----

* Added support for SKART.
* This required a bump to the dependencies of processors to 2.3.1.
  This was required to align sdp-config dependencies and more importantly
  to ensure the bugs for the default values of TELESCOPE and OBSERVER
  have been picked up.

0.3.1
-----

* Added a processor that averages the visibility data in time.
  This processor is intended to be used in conjunction with the
  aggregation processor or the payload aggregation provided by the
  receiver.

0.3.0
-----

* Added support for multiple receivers in the `sdp-processor` entrypoint.
* Dependency ska-sdp-realtime-receive-processors updated to 2.3.0. This picks
  up the sdp-config 0.9 requirements.


0.2.4
-----
* Bumped the version dependency of the processors. This picks up some changed behaviour
  in the runner: namely the buffereing of start and stop scans. Plus a forced ordering
  of the plasma_refs for all processors that combine Visibility objects.
  
0.2.3
-----

Added
^^^^^

* Added example Jupyter notebook for running multiple processors.
* Added modular processor runner entrypoint `sdp-processor` with OCI image.
* Added example processors to `ska_sdp_realtime_receive_integration.processors`.
* Added Changelog.
