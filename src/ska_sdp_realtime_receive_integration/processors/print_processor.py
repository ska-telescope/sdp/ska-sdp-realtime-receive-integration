"""Example PrintProcessor implementation"""

from __future__ import annotations

from typing import Iterable

from realtime.receive.processors.sdp.base_processor import BaseProcessor
from ska_sdp_datamodels.visibility.vis_model import Visibility


class PrintProcessor(BaseProcessor):
    """A processor that prints datasets, useful for tests"""

    @staticmethod
    def create(_argv: Iterable[str]) -> PrintProcessor:
        return PrintProcessor()

    async def process(self, dataset: Visibility) -> Visibility:
        print(dataset)
        return dataset

    async def close(self):
        pass
