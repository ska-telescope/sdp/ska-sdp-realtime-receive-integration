"""
main for realtime.receive.processors.
"""

import argparse
import asyncio
import functools
import logging
import signal
from ast import literal_eval

import ska_ser_logging
from realtime.receive.core import ChannelRange
from realtime.receive.core.common import strtobool
from realtime.receive.processors.runner import Runner
from realtime.receive.processors.sdp import BaseProcessor
from realtime.receive.processors.utils.processor_utils import load_base_processor_class
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline, listify

logger = logging.getLogger(__name__)


def load_processors(
    processor_classes: list[str], processor_args: list[str]
) -> list[BaseProcessor]:
    """Loader for processors from a config and command line arguments.

    Args:
        processor_classes (list[str]): list of class import paths.
        processor_args (list[str]): combined list of arguments for all processors.

    Returns:
        list[BaseProcessor]: list of loaded processors.
    """
    return [
        load_base_processor_class(processor_class).create(processor_args)
        for processor_class in processor_classes
    ]


def main():
    """
    Main function.
    """

    parser = _build_parser()
    args, processor_args = parser.parse_known_args()
    if args.verbose is not None:
        ska_ser_logging.configure_logging(level=args.verbose.upper())

    user_processors = load_processors(
        listify(literal_eval(args.processor_classes)), processor_args
    )

    async def amain():
        if args.test_input:
            emulated_sdp_pipeline_task = asyncio.create_task(
                arun_emulated_sdp_pipeline(
                    input_ms_filepath=args.test_input,
                    plasma_socket=args.plasma_socket,
                    mem_size=1_000_000_000,
                    user_processors=user_processors,
                    num_receivers=1,
                    readiness_file=args.readiness_file,
                    polling_rate=1,
                    channels=args.input_channel_range,
                )
            )

            def shutdown(signame: signal.Signals):
                logger.warning(
                    "%s Signal received, stopping SDP pipeline emulation..",
                    signame.name,
                )
                emulated_sdp_pipeline_task.cancel()

            loop = asyncio.get_running_loop()
            for signame in (signal.SIGINT, signal.SIGTERM):
                loop.add_signal_handler(signame, functools.partial(shutdown, signame))
            try:
                await emulated_sdp_pipeline_task
            except asyncio.CancelledError:
                pass
        else:
            runner = Runner(
                plasma_socket=args.plasma_socket,
                user_processors=user_processors,
                num_receivers=args.num_receivers,
                readiness_file=args.readiness_file,
                polling_rate=1,
                use_sdp_metadata=args.use_sdp_metadata,
            )

            async def shutdown(signame: signal.Signals):
                logger.warning("%s Signal received, stopping runner..", signame.name)
                await runner.finish_and_close()

            loop = asyncio.get_running_loop()
            for signame in (signal.SIGINT, signal.SIGTERM):
                loop.add_signal_handler(
                    signame,
                    lambda signame=signame: asyncio.create_task(shutdown(signame)),
                )
            await runner.run()

    asyncio.run(amain())


def _build_parser():
    parser = argparse.ArgumentParser(description="Runs a modular receive processor pipeline")
    parser.add_argument("processor_classes", help="The classes implementing the Processor")
    parser.add_argument(
        "-v",
        "--verbose",
        default=None,
        help="If set, more verbose output will be produced",
    )
    parser.add_argument(
        "-s",
        "--plasma_socket",
        help="The socket where Plasma is listening for connections",
        default="/tmp/plasma",
    )
    parser.add_argument(
        "-r",
        "--readiness-file",
        help=(
            "An empty file that will be created after the processor has "
            "finished setting up, signalling it's ready to receive data"
        ),
    )
    parser.add_argument(
        "--max-scans",
        type=int,
        default=None,
        help="The number of scans to process data for before automatically exiting.",
    )
    parser.add_argument(
        "--test-input",
        type=str,
        default=None,
        help="Emulate data reception using a measurement set file",
    )
    parser.add_argument(
        "--input-channel-range",
        type=ChannelRange.from_str,
        default=None,
        help=(
            "Map the channel data in the measurement set to the "
            "specified channel ids using start:count[:stride] notation."
        ),
    )
    parser.add_argument(
        "--use-sdp-metadata",
        dest="use_sdp_metadata",
        type=strtobool,
        default=True,
        help="Use SDP metadata support, ignored when used with --input",
    )
    parser.add_argument(
        "--num-receivers",
        dest="num_receivers",
        type=int,
        default=1,
        help="Number of receivers to use for the pipeline",
    )
    return parser


if __name__ == "__main__":
    main()
