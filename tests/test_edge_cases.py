"""
Visibility receive tests exploring the scalability of the
emulator and receiver in isolation.
"""

from pathlib import Path

import pytest
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor

from tests.common_vis_receive_pipeline import VisReceivePipeline, VisReceivePipelineConfig


@pytest.mark.xfail(reason="YAN-1581: Unbalanced channels are not gracefully handled")
def test_unbalanced_streams(output_ms_path: Path):
    config = VisReceivePipelineConfig(num_streams=3)
    config.with_hardcoded_metadata(6, ChannelRange(0, 16), 10, include_autos=True)

    processor = MSWriterProcessor(output_ms_path)
    with VisReceivePipeline(config, processor) as vis_receive:
        vis_receive.run()


def test_single_channel_spectral_window(output_ms_path: Path):
    config = VisReceivePipelineConfig()
    config.with_hardcoded_metadata(6, ChannelRange(0, 1), 10)

    processor = MSWriterProcessor(output_ms_path)
    with VisReceivePipeline(config, processor) as vis_receive:
        vis_receive.run()
