"""
Visibility recieve tests using the CBF emulator hardcoded mode
"""

from pathlib import Path

import numpy as np
import pytest
from aiostream import stream
from numpy.testing import assert_almost_equal
from realtime.receive.core.baseline_utils import baselines
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.msutils import MeasurementSet, vis_reader
from realtime.receive.core.time_utils import mjd_to_unix
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor

from tests.common_vis_receive_pipeline import VisReceivePipeline, VisReceivePipelineConfig


@pytest.mark.parametrize("num_stations", [5])
@pytest.mark.parametrize("include_autos", [True, False])
@pytest.mark.parametrize("num_timesteps", [150])
@pytest.mark.parametrize(
    "channels",
    [
        ChannelRange(0, 50, 3),
        ChannelRange(2000, 100, 4),
    ],
)
@pytest.mark.asyncio
# pylint: disable-next=too-many-locals
async def test_hardcoded_values_traverse_pipeline(
    output_ms_path: Path,
    num_stations: int,
    include_autos: bool,
    num_timesteps: int,
    channels: ChannelRange,
):
    config = _build_hardcoded_config(num_stations, include_autos, num_timesteps, channels)

    # Single payload enforced by negative receiver aggregation time_period
    mswriter_processor = MSWriterProcessor(output_ms_path)
    with VisReceivePipeline(config, mswriter_processor) as vis_receive:
        vis_receive.run()

    gen_path = f"{output_ms_path.parent}/{output_ms_path.stem}.scan-1{output_ms_path.suffix}"
    with MeasurementSet.open(gen_path) as output_ms:
        scan_type = output_ms.calculate_scan_type()
        assert len(scan_type.beams) == 1
        assert len(scan_type.beams[0].channels.spectral_windows) == 1
        spectral_window = scan_type.beams[0].channels.spectral_windows[0]

        # The MS doesn't have a concept of a channel_id, so we lose that information.
        # As a result, it will always start from 0.
        assert 0 == spectral_window.start
        assert channels.count == spectral_window.count
        assert channels.stride == spectral_window.stride

        hc_config = config.sender.hardcoded
        num_baselines = baselines(num_stations, include_autos)
        # pylint: disable-next=no-member
        async with stream.enumerate(vis_reader(output_ms)).stream() as vis_data_stream:
            async for i, (vis, timestamp) in vis_data_stream:
                expected_time = hc_config.visibility_epoch + i * hc_config.visibility_interval
                assert_almost_equal(mjd_to_unix(timestamp), expected_time, decimal=4)
                assert vis.shape == (num_baselines, channels.count, hc_config.num_polarisations)
                assert np.all(vis == 0)


def _build_hardcoded_config(
    num_stations: int,
    include_autos: bool,
    num_timesteps: int,
    channels: ChannelRange,
):
    config = VisReceivePipelineConfig()
    config.with_hardcoded_metadata(
        num_stations, channels, num_timesteps, include_autos=include_autos
    )
    config.with_hasty_udp()
    return config
