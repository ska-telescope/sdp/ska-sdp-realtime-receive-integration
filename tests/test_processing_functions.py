"""
MSWriterProcessor integration tests
"""

import logging
import tarfile
import tempfile
from pathlib import Path

import numpy as np
import pytest
from realtime.receive.core.ms_asserter import AssertProps, MSAsserter
from realtime.receive.processors.file_executor import FunctionFileExecutor
from realtime.receive.processors.sdp.ms_event_handler import FileExecutorMsClosedEventHander
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline

from ska_sdp_realtime_receive_integration.processors.average_processor import AverageTimeProcessor
from ska_sdp_realtime_receive_integration.processors.rfi_mask_processor import RfiMaskProcessor

PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)


def untar(archive_path: str | Path) -> Path:
    """Extracts a tar archive to the same directory as the archive.

    Returns:
        Path: string name of the extracted directory
    """
    archive_path = Path(archive_path)
    with tarfile.open(archive_path, "r:gz") as tar:
        tar.extractall(archive_path.parent)
        return archive_path.parent / tar.getnames()[0]


@pytest.mark.asyncio
@pytest.mark.xfail(reason="mswriter does not yet write flags")
async def test_rfi_mask_pipeline():
    """Tests RFI masking with mswriter processor."""

    input_ms_path = untar("data/sim-vis.ms.tar.gz")
    expected_ms_path = untar("data/sim-vis-flagged.ms.tar.gz")
    output_ms_paths = []

    def append_ms(ms_path: str):
        output_ms_paths.append(ms_path)

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("sim-vis-output.ms")

        processors = [
            RfiMaskProcessor(rfi_mask=np.array([[1.498e8, 1.499e8]])),
            MSWriterProcessor(
                output_ms_template,
                timestamp_output=True,
                event_handlers=[FileExecutorMsClosedEventHander(FunctionFileExecutor(append_ms))],
            ),
        ]

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            input_ms_path,
            PLASMA_SOCKET,
            50000000,
            processors,
            num_scans=1,
        )

        asserter = MSAsserter()
        for output_ms_path in output_ms_paths:
            asserter.assert_ms_equal(
                str(expected_ms_path),
                output_ms_path,
                columns=[
                    AssertProps("DATA", True, True),
                    AssertProps("FLAG", True, True),
                    AssertProps("ANTENNA1", True, True),
                    AssertProps("ANTENNA2", True, True),
                ],
                column_desc_keys=[
                    "ndim",
                    "valueType",
                    "_c_order",
                    "maxlen",
                ],
            )


@pytest.mark.asyncio
async def test_average_time_pipeline():
    """Tests RFI masking with mswriter processor."""

    input_ms_path = untar("data/sim-vis.ms.tar.gz")
    expected_ms_path = untar("data/sim-vis-flagged.ms.tar.gz")
    output_ms_paths = []

    def append_ms(ms_path: str):
        output_ms_paths.append(ms_path)

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("sim-vis-output.ms")

        processors = [
            AverageTimeProcessor(timestep=1, flag_threshold=0.5),
            MSWriterProcessor(
                output_ms_template,
                timestamp_output=True,
                event_handlers=[FileExecutorMsClosedEventHander(FunctionFileExecutor(append_ms))],
            ),
        ]

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            input_ms_path,
            PLASMA_SOCKET,
            50000000,
            processors,
            num_scans=1,
        )

        asserter = MSAsserter()
        for output_ms_path in output_ms_paths:
            asserter.assert_ms_equal(
                str(expected_ms_path),
                output_ms_path,
                columns=[
                    AssertProps("DATA", True, True),
                    # AssertProps("FLAG", False, False),
                    AssertProps("ANTENNA1", True, True),
                    AssertProps("ANTENNA2", True, True),
                ],
                column_desc_keys=[
                    "ndim",
                    "valueType",
                    "_c_order",
                    "maxlen",
                ],
            )
