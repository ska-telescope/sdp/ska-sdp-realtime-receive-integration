"""Shared, test-suite wide fixtures"""

import tempfile
from pathlib import Path

import pytest


@pytest.fixture(name="output_ms_path")
def output_ms_path():
    """
    Provides a temporary, empty folder named as an MS to that will be
    removed after the test.
    """
    with tempfile.NamedTemporaryFile(prefix="output_", suffix=".ms") as file:
        yield Path(file.name)
