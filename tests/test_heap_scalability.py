"""
Visibility receive tests exploring the scalability of the
emulator and receiver in isolation.
"""

import pytest
from pytest_benchmark.fixture import BenchmarkFixture
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.icd import Telescope

from tests.common_vis_receive_pipeline import VisReceivePipeline, VisReceivePipelineConfig


@pytest.mark.benchmark(group="heap_scaling")
@pytest.mark.parametrize(
    "telescope, num_stations, channel_count",
    # Numbers taken from https://confluence.skatelescope.org/display/SWSI/Array+Assemblies
    # Commented out scenarios need more memory than most laptops and CI runners
    # can comfortably provide.
    [
        (Telescope.LOW, 6, 13824),  # AA0.5
        (Telescope.LOW, 18, 13824),  # AA1
        (Telescope.LOW, 64, 3456),  # AA2 limited bandwidth
        # (Telescope.LOW, 64, 27648), # AA2 full bandwidth
        # (Telescope.LOW, 307, 55296), # AA*
        (Telescope.MID, 4, 16384),  # AA0.5
        (Telescope.MID, 8, 16384),  # AA1
        (Telescope.MID, 68, 3456),  # AA2 limited bandwidth
        # (Telescope.MID, 68, 16384), # AA2 full bandwidth
        # (Telescope.MID, 144, 65536), # AA*
    ],
)
def test_spead_heap_scaling(
    benchmark: BenchmarkFixture,
    telescope: Telescope,
    num_stations: int,
    channel_count: int,
):
    config = VisReceivePipelineConfig(num_streams=8)
    config.sender_spead2_config.telescope = telescope
    config.with_fast_tcp()
    config.with_max_packet_size(63000)
    config.with_hardcoded_metadata(
        num_stations,
        ChannelRange(0, channel_count),
        10,
        include_autos=True,
    )

    def run():
        with VisReceivePipeline(config) as vis_receive:
            vis_receive.run(timeout=60)

    benchmark.pedantic(run)
