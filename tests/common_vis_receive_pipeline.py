"""
Code to encapsulate the running of a full visibility receive pipeline
"""

import asyncio
import logging
import multiprocessing
import queue
import re
import subprocess
from concurrent import futures
from contextlib import AbstractContextManager, contextmanager, nullcontext
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import Callable, Coroutine

from pyinstrument import Profiler
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.modules.consumers.plasma_writer import PlasmaWriterConfig
from realtime.receive.modules.receiver import ReceiverConfig, receive
from realtime.receive.modules.receivers.spead2_receivers import (
    Spead2ReceptionConfig,
    TransportProtocols,
)
from realtime.receive.processors.runner import Runner
from realtime.receive.processors.sdp.base_processor import BaseProcessor
from ska_sdp_cbf_emulator.data_source import MeasurementSetDataSourceConfig
from ska_sdp_cbf_emulator.data_source.hardcoded import HardcodedDataSourceConfig
from ska_sdp_cbf_emulator.packetiser import SenderConfig, packetise
from ska_sdp_cbf_emulator.transmitters.spead2_transmitters import (
    Spead2TransmissionConfig,
    TransportProtocol,
)

logger = logging.getLogger(__name__)

PLASMA_SOCKET = "/tmp/plasma"
PROFILING_PATH = ".profiles"


async def _wrap_blocking_call(callback, *args):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(None, callback, *args)


class PipelineComponent(Enum):
    """Enumeration of each component of a VisReceivePipeline"""

    SENDER = 1
    RECEIVER = 2
    PROCESSOR = 3


class VisReceivePipelineConfig:
    """
    Encapsulates all configuration available to the
    Visibility Receive pipeline.
    """

    def __init__(self, num_streams=1):
        self.sender = SenderConfig()
        self.sender.transmission = Spead2TransmissionConfig(num_streams=num_streams)

        self.receiver = ReceiverConfig()
        self.receiver.reception = Spead2ReceptionConfig(num_streams=num_streams)
        self.receiver.consumer = PlasmaWriterConfig(plasma_path=PLASMA_SOCKET)

        # TODO(YAN-1506) The default (Katpoint) slows these the tests
        # down dramatically. Let's use a faster one by default.
        self.receiver.uvw_engine.engine = "measures"

        self.max_scans = 1

        self.profiled_components: set[PipelineComponent] = set()

    def with_hardcoded_metadata(
        self,
        num_stations: int,
        channels: ChannelRange,
        num_timesteps: int,
        include_autos: bool = True,
    ) -> None:
        """
        Returns a pipeline configuration where both the sender and the receiver
        are configured with the given, hardcoded metadata.
        """

        self.sender.hardcoded = HardcodedDataSourceConfig(
            num_stations=num_stations,
            channels=channels,
            num_timesteps=num_timesteps,
            include_autocorrelations=include_autos,
        )

        self.receiver.scan_provider.hardcoded_spectral_window_channels = channels

        self.receiver.tm.hardcoded_num_receptors = num_stations
        self.receiver.tm.hardcoded_auto_corr = include_autos

    def with_fast_tcp(self) -> None:
        """
        Modifies the pipeline configuration so that data flows as fast as
        possible through it via TCP.
        """
        self.sender.time_interval = -1
        self.sender_spead2_config.rate = 0
        self.sender_spead2_config.transport_protocol = TransportProtocol.TCP
        self.receiver_spead2_config.transport_protocol = TransportProtocols.TCP

    def with_hasty_udp(self, udp_rate: int | None = None) -> None:
        """
        Modifies the pipeline configuration so that data is exchanged via UDP
        without any waiting time between integration intervals, and sending
        data at the given rate *per stream* from the sender.
        """
        self.sender.time_interval = -1
        if udp_rate is not None:
            self.sender_spead2_config.rate = udp_rate
        self.sender_spead2_config.transport_protocol = TransportProtocol.UDP
        self.receiver_spead2_config.transport_protocol = TransportProtocols.UDP

    def with_heap_capacity(self, num_heaps: int) -> None:
        """
        Modifies the pipeline configuration so that the receiver end is able to
        hold the given number of heaps per stream.
        """
        self.plasma_writer_config.payloads_in_flight = num_heaps
        self.receiver_spead2_config.ring_heaps = num_heaps

    def with_max_packet_size(self, size: int) -> None:
        """Sets the maximum packet size for the sender and the receiver."""
        self.sender_spead2_config.max_packet_size = size
        self.receiver_spead2_config.max_packet_size = size

    @property
    def sender_spead2_config(self) -> Spead2TransmissionConfig:
        """Get the sender transmission config as a :class:`Spead2TransmissionConfig`"""
        assert isinstance(self.sender.transmission, Spead2TransmissionConfig)
        return self.sender.transmission

    @property
    def receiver_spead2_config(self) -> Spead2ReceptionConfig:
        """Get the receiver reception config as a :class:`Spead2ReceptionConfig`"""
        assert isinstance(self.receiver.reception, Spead2ReceptionConfig)
        return self.receiver.reception

    @property
    def plasma_writer_config(self) -> PlasmaWriterConfig:
        """Get the receiver consumer config as a :class:`PlasmaWriterConfig`"""
        assert isinstance(self.receiver.consumer, PlasmaWriterConfig)
        return self.receiver.consumer

    @property
    def ms_path(self):
        """The path to a measurement set to push through the pipeline"""
        assert self.receiver.tm.measurement_set == self.receiver.scan_provider.measurement_set
        return self.receiver.tm.measurement_set

    @ms_path.setter
    def ms_path(self, ms_path: str):
        self.receiver.tm.measurement_set = ms_path
        self.receiver.scan_provider.measurement_set = ms_path

        if isinstance(self.sender.ms, MeasurementSetDataSourceConfig):
            self.sender.ms.location = ms_path
        else:
            self.sender.ms = MeasurementSetDataSourceConfig(ms_path)


class VisReceivePipeline(AbstractContextManager):
    """
    Encapsulates the setup and running of a Visibility receive pipeline
    """

    def __init__(
        self,
        config: VisReceivePipelineConfig,
        processor: BaseProcessor | None = None,
        mem_store_size_bytes=20_000_000,
    ):
        self._config = config
        self._manager = None
        self._mem_store_size_bytes = mem_store_size_bytes
        self._mem_store_proc = None
        self._processor = processor
        if not self._processor:
            config.receiver.consumer.name = "null_consumer"

        self._run_num = self._get_next_run_num()
        self._profiling_time = datetime.now()

    def __enter__(self):
        self._manager = multiprocessing.Manager()
        if self._processor:
            self._mem_store_proc = subprocess.Popen(
                ["plasma_store", "-m", str(self._mem_store_size_bytes), "-s", PLASMA_SOCKET]
            )
        return self

    def run(self, timeout=20):
        """
        Run the visibility receive pipeline with data being
        processed by the given processor.
        """
        receiver_ready_evt = self._manager.Event()
        processor_ready_evt = self._manager.Event()
        procs = [
            _ProcessExecutor(self._run_receiver, receiver_ready_evt, processor_ready_evt),
            _ProcessExecutor(self._run_sender, receiver_ready_evt),
        ]
        if self._processor:
            procs.append(_ProcessExecutor(self._run_processor, processor_ready_evt))
        else:
            processor_ready_evt.set()

        futs = [proc.start() for proc in procs]
        done, waiting = futures.wait(futs, timeout=timeout, return_when=futures.FIRST_EXCEPTION)
        for proc in procs:
            proc.stop()

        # raise first exception, if any
        for task in done:
            if task.exception() is not None:
                task.result()
        if len(done) != len(futs) or waiting:
            raise RuntimeError("Pipeline did not complete before timeout")

    def __exit__(self, __exc_type, __exc_value, __traceback):
        if self._mem_store_proc:
            self._mem_store_proc.terminate()
            self._mem_store_proc.wait()

        self._manager.shutdown()
        self._manager = None

    async def _run_processor(self, ready_evt: multiprocessing.Event):
        readiness_file = Path(".processor_ready")
        readiness_file.unlink(missing_ok=True)

        task = asyncio.create_task(self._set_event_when_file_exists(ready_evt, readiness_file))

        with self._instrumentation(PipelineComponent.PROCESSOR):
            processor_runner = Runner(
                PLASMA_SOCKET,
                [self._processor],
                polling_rate=0.001,
                use_sdp_metadata=False,
                readiness_file=str(readiness_file),
                max_scans=self._config.max_scans,
            )
            await processor_runner.run()

        await task
        readiness_file.unlink()

    async def _run_receiver(
        self,
        receiver_ready_evt: multiprocessing.Event,
        processor_ready_evt: multiprocessing.Event,
    ):
        processor_ready_evt.wait()

        with self._instrumentation(PipelineComponent.RECEIVER):
            await receive(self._config.receiver, receiver_ready_evt=receiver_ready_evt)

    async def _run_sender(self, receiver_ready_evt: multiprocessing.Event):
        receiver_ready_evt.wait()

        with self._instrumentation(PipelineComponent.SENDER):
            await packetise(self._config.sender)

    async def _set_event_when_file_exists(self, evt: multiprocessing.Event, path: Path):
        while not await _wrap_blocking_call(path.exists):
            await asyncio.sleep(0.1)
        evt.set()

    def _instrumentation(self, component: PipelineComponent):
        return (
            self._instrumentation_context(component)
            if component in self._config.profiled_components
            else nullcontext()
        )

    @contextmanager
    def _instrumentation_context(self, component: PipelineComponent):
        profiler = Profiler()
        profiler.start()
        yield
        profiler.stop()

        filename = (
            f"{self._run_num:04}_{self._profiling_time.isoformat(timespec='seconds')}"
            f".{component.name.lower()}.html"
        )
        full_path = Path(PROFILING_PATH) / filename
        profiler.write_html(full_path)

    def _get_next_run_num(self):
        path = Path(PROFILING_PATH)
        path.mkdir(parents=True, exist_ok=True)

        profile_pattern = re.compile(r"(\d{4})_.*\.html")
        existing_run_nums = [
            int(file.name[:4]) for file in path.iterdir() if profile_pattern.match(file.name)
        ]
        next_run_num = max(existing_run_nums, default=0) + 1
        return next_run_num


class _ProcessExecutor:
    def __init__(self, target: Callable[..., Coroutine], *args, **kwargs):
        # We can't use a multiprocessing.ProcessPoolExecutor as args need
        # to be picklable, and the MSWriterProcessor is not picklable.
        self._process = multiprocessing.Process(
            target=self._run,
            args=(target, *args),
            kwargs=kwargs,
        )
        self._result_queue = multiprocessing.Queue(maxsize=1)
        self._result_executor = futures.ThreadPoolExecutor(max_workers=1)

    def _run(self, target: Callable[..., Coroutine], *args, **kwargs):
        try:
            ret = target(*args, **kwargs)
            assert asyncio.iscoroutine(ret)
            ret = asyncio.run(ret)
            self._result_queue.put((ret, None))
        except Exception as exc:  # pylint: disable=broad-exception-caught
            # We lose the stack trace if we don't log here
            logger.exception(exc)
            self._result_queue.put((None, exc))

    def start(self):
        """
        Start the process, returning a Future that will complete
        when the process has finished processing or thrown an exception.
        """
        self._process.start()
        return self._result_executor.submit(self._consume_process_result)

    def _consume_process_result(self):
        while self._process.is_alive():
            try:
                res, exc = self._result_queue.get(timeout=0.2)
                if exc is not None:
                    raise exc

                return res
            except queue.Empty:
                pass

        raise ProcessLookupError("Process has been killed before it was able to complete")

    def stop(self):
        """
        If the process hasn't already completed, terminate it
        then wait for it to exit.
        """
        try:
            self._process.terminate()
            self._process.join(timeout=5)
        except TimeoutError:
            self._process.kill()
            self._process.join()

        self._result_executor.shutdown(cancel_futures=True)
