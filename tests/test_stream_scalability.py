"""
Benchmarks for the receiver pipeline along the streams/channels dimension.
"""

import pytest
from pytest_benchmark.fixture import BenchmarkFixture
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.icd import Telescope

from tests.common_vis_receive_pipeline import VisReceivePipeline, VisReceivePipelineConfig


@pytest.mark.benchmark(group="stream_scalability")
@pytest.mark.parametrize(
    "num_streams, channels_per_stream, num_stations, telescope",
    (
        (100, 1, 6, Telescope.LOW),
        (100, 20, 4, Telescope.MID),
    ),
)
def test_stream_scalability(
    benchmark: BenchmarkFixture,
    num_streams: int,
    channels_per_stream: int,
    num_stations: int,
    telescope: str,
):
    config = _build_stream_centric_config(
        num_streams, channels_per_stream, num_stations, telescope
    )

    def run():
        with VisReceivePipeline(config) as pipeline:
            pipeline.run(timeout=100)

    benchmark.pedantic(run)


def _build_stream_centric_config(
    num_streams: int,
    channels_per_stream: int,
    num_stations: int,
    telescope: Telescope,
) -> VisReceivePipelineConfig:
    config = VisReceivePipelineConfig(num_streams=num_streams)
    config.sender_spead2_config.channels_per_stream = channels_per_stream
    config.sender_spead2_config.telescope = telescope

    channels = ChannelRange(0, num_streams * channels_per_stream)
    config.with_hardcoded_metadata(num_stations, channels, num_timesteps=120)
    config.with_fast_tcp()

    # We start at port 10000 (default is 21000) to lower the changes of
    # trying to bind a port already in use.
    config.receiver_spead2_config.port_start = 10000
    config.sender_spead2_config.target_port_start = 10000

    return config
