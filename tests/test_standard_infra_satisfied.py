"""
Stub tests to maintain the illusion to standard CI tools that we
contain real code in src/
"""

import ska_sdp_realtime_receive_integration


def test_main_module_is_imported():
    """Without importing the main module, coverage tools give a warning"""
    assert ska_sdp_realtime_receive_integration
