"""
Simple tests to verify the Visibility Receive pipeline
"""

import pathlib

from realtime.receive.core import ms_asserter
from realtime.receive.core.common import untar
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor

from tests.common_vis_receive_pipeline import VisReceivePipeline, VisReceivePipelineConfig

INPUT_FILE = "data/sim-vis.ms"
NUM_TIMESTAMPS = 133


def test_ms_is_preserved_through_pipeline(output_ms_path: pathlib.Path):
    untar(INPUT_FILE)

    config = VisReceivePipelineConfig(num_streams=1)
    config.with_hasty_udp(14700000)
    config.ms_path = INPUT_FILE

    mswriter_processor = MSWriterProcessor(output_ms_path.name)
    with VisReceivePipeline(config, mswriter_processor) as vis_receive:
        vis_receive.run()

    asserter = ms_asserter.MSAsserter()
    path = pathlib.Path(output_ms_path.name)
    gen_path = f"{path.parent}/{path.stem}.scan-1{path.suffix}"
    asserter.assert_ms_data_equal(INPUT_FILE, gen_path)
